-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 23, 2019 at 05:48 PM
-- Server version: 5.7.24
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `libertasapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `gradska`
--

DROP TABLE IF EXISTS `gradska`;
CREATE TABLE IF NOT EXISTS `gradska` (
  `tip_karte` int(11) NOT NULL,
  `cijena` int(11) NOT NULL,
  UNIQUE KEY `tip_karte` (`tip_karte`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

--
-- Dumping data for table `gradska`
--

INSERT INTO `gradska` (`tip_karte`, `cijena`) VALUES
(1, 10),
(2, 40),
(3, 120);

-- --------------------------------------------------------

--
-- Table structure for table `karta`
--

DROP TABLE IF EXISTS `karta`;
CREATE TABLE IF NOT EXISTS `karta` (
  `id_karte` int(11) NOT NULL AUTO_INCREMENT,
  `cijena` int(11) NOT NULL,
  `ukupno` int(11) NOT NULL,
  `tip_karte` int(11) NOT NULL,
  `pdv` int(11) NOT NULL,
  PRIMARY KEY (`id_karte`),
  KEY `tip_karte` (`tip_karte`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

--
-- Dumping data for table `karta`
--

INSERT INTO `karta` (`id_karte`, `cijena`, `ukupno`, `tip_karte`, `pdv`) VALUES
(2, 10, 35, 1, 25),
(4, 10, 35, 1, 25),
(5, 10, 35, 1, 25),
(6, 10, 35, 1, 25),
(7, 10, 35, 1, 25),
(8, 10, 35, 1, 25),
(9, 10, 35, 1, 25),
(10, 10, 35, 1, 25),
(21, 10, 35, 1, 25),
(37, 10, 35, 1, 25),
(38, 10, 35, 1, 25),
(39, 40, 65, 1, 25),
(40, 40, 65, 2, 25),
(47, 10, 35, 1, 25),
(48, 40, 65, 2, 25),
(49, 10, 35, 1, 25),
(51, 10, 35, 1, 25),
(52, 10, 35, 1, 25),
(53, 10, 35, 1, 25),
(54, 50, 75, 2, 25),
(56, 50, 75, 2, 25),
(57, 10, 35, 1, 25),
(58, 10, 35, 1, 25),
(59, 0, 0, 1, 0),
(60, 0, 0, 1, 0),
(61, 0, 0, 1, 0),
(62, 10, 35, 1, 25),
(63, 40, 50, 1, 10),
(64, 40, 50, 1, 10),
(65, 40, 50, 1, 10),
(66, 40, 50, 1, 10),
(67, 40, 50, 1, 10),
(71, 10, 35, 1, 25),
(74, 50, 75, 3, 25),
(75, 10, 35, 1, 25),
(76, 40, 50, 4, 10),
(77, 50, 75, 3, 25),
(78, 40, 50, 4, 10),
(80, 40, 50, 4, 10),
(81, 40, 50, 4, 10),
(82, 10, 35, 1, 25);

--
-- Triggers `karta`
--
DROP TRIGGER IF EXISTS `tr1`;
DELIMITER $$
CREATE TRIGGER `tr1` AFTER INSERT ON `karta` FOR EACH ROW INSERT INTO transakcije (id_karte,Cijena,Ukupno,pdv)
SELECT id_karte,cijena,ukupno,pdv from karta 
where id_karte=(SELECT MAX(id_karte)from karta)
group by id_karte
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `mjesecna`
--

DROP TABLE IF EXISTS `mjesecna`;
CREATE TABLE IF NOT EXISTS `mjesecna` (
  `broj_pokaza` int(11) NOT NULL AUTO_INCREMENT,
  `tip_karte` int(11) NOT NULL DEFAULT '4',
  `ime` text COLLATE utf8_croatian_ci NOT NULL,
  `prez` text COLLATE utf8_croatian_ci NOT NULL,
  `OIB` int(11) NOT NULL,
  `datum_izdavanja` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `datum_trajanja` datetime DEFAULT NULL,
  PRIMARY KEY (`broj_pokaza`),
  UNIQUE KEY `broj_pokaza` (`broj_pokaza`),
  KEY `tip_karte` (`tip_karte`),
  KEY `id_karte` (`tip_karte`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

--
-- Dumping data for table `mjesecna`
--

INSERT INTO `mjesecna` (`broj_pokaza`, `tip_karte`, `ime`, `prez`, `OIB`, `datum_izdavanja`, `datum_trajanja`) VALUES
(1, 4, 'Ivica', 'Curcija', 205215125, '2019-01-23 18:18:44', '2019-02-22 18:18:44'),
(3, 4, 'Pero', 'Perica', 23512165, '2019-01-23 18:25:23', '2019-02-22 18:25:23'),
(4, 4, 'Mare', 'Perica', 52151125, '2019-01-23 18:25:55', '2019-02-22 18:25:55');

--
-- Triggers `mjesecna`
--
DROP TRIGGER IF EXISTS `mjesecna`;
DELIMITER $$
CREATE TRIGGER `mjesecna` AFTER INSERT ON `mjesecna` FOR EACH ROW INSERT INTO `karta`( `cijena`, `Ukupno`, `tip_karte`, `pdv`) VALUES (40,50,4,10)
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `test_trigger`;
DELIMITER $$
CREATE TRIGGER `test_trigger` BEFORE INSERT ON `mjesecna` FOR EACH ROW SET
    NEW.datum_trajanja = TIMESTAMPADD(DAY, 30, NEW.datum_izdavanja)
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `mjesto`
--

DROP TABLE IF EXISTS `mjesto`;
CREATE TABLE IF NOT EXISTS `mjesto` (
  `ime_mjesta` int(11) NOT NULL,
  `id_mjesta` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_mjesta`),
  UNIQUE KEY `id_mjesta` (`id_mjesta`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `prigradska`
--

DROP TABLE IF EXISTS `prigradska`;
CREATE TABLE IF NOT EXISTS `prigradska` (
  `id_odredista` int(11) NOT NULL,
  `cijena` int(11) NOT NULL,
  `id_polazista` int(11) NOT NULL,
  `tip_karte` int(11) NOT NULL,
  PRIMARY KEY (`tip_karte`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `transakcije`
--

DROP TABLE IF EXISTS `transakcije`;
CREATE TABLE IF NOT EXISTS `transakcije` (
  `id_transakcije` int(11) NOT NULL AUTO_INCREMENT,
  `id_karte` int(11) NOT NULL DEFAULT '100',
  `Cijena` int(11) NOT NULL,
  `pdv` int(11) NOT NULL,
  `Ukupno` int(11) NOT NULL,
  `Datum` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_transakcije`),
  UNIQUE KEY `id_karte` (`id_karte`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

--
-- Dumping data for table `transakcije`
--

INSERT INTO `transakcije` (`id_transakcije`, `id_karte`, `Cijena`, `pdv`, `Ukupno`, `Datum`) VALUES
(9, 37, 10, 25, 35, '2019-01-22 21:00:16'),
(10, 38, 10, 25, 35, '2019-01-22 21:56:45'),
(11, 39, 40, 25, 65, '2019-01-22 22:00:43'),
(12, 40, 40, 25, 65, '2019-01-22 22:01:13'),
(13, 47, 10, 25, 35, '2019-01-22 23:01:09'),
(14, 48, 40, 25, 65, '2019-01-22 23:01:15'),
(15, 49, 10, 25, 35, '2019-01-22 23:02:03'),
(16, 51, 10, 25, 35, '2019-01-22 23:24:25'),
(17, 52, 10, 25, 35, '2019-01-22 23:24:29'),
(18, 53, 10, 25, 35, '2019-01-22 23:24:51'),
(19, 54, 50, 25, 75, '2019-01-22 23:30:31'),
(20, 56, 50, 25, 75, '2019-01-23 00:00:26'),
(21, 57, 10, 25, 35, '2019-01-23 00:10:28'),
(22, 58, 10, 25, 35, '2019-01-23 00:18:48'),
(23, 59, 0, 0, 0, '2019-01-23 00:19:58'),
(24, 60, 0, 0, 0, '2019-01-23 15:33:00'),
(25, 61, 0, 0, 0, '2019-01-23 15:35:33'),
(26, 62, 10, 25, 35, '2019-01-23 15:37:05'),
(27, 63, 40, 10, 50, '2019-01-23 16:54:50'),
(28, 64, 40, 10, 50, '2019-01-23 16:54:59'),
(29, 65, 40, 10, 50, '2019-01-23 16:55:05'),
(30, 66, 40, 10, 50, '2019-01-23 16:56:02'),
(31, 67, 40, 10, 50, '2019-01-23 16:57:17'),
(32, 71, 10, 25, 35, '2019-01-23 17:25:42'),
(33, 74, 50, 25, 75, '2019-01-23 17:45:49'),
(34, 75, 10, 25, 35, '2019-01-23 17:45:52'),
(35, 76, 40, 10, 50, '2019-01-23 17:49:44'),
(36, 77, 50, 25, 75, '2019-01-23 18:00:40'),
(37, 78, 40, 10, 50, '2019-01-23 18:18:44'),
(39, 80, 40, 10, 50, '2019-01-23 18:25:23'),
(40, 81, 40, 10, 50, '2019-01-23 18:25:55'),
(41, 82, 10, 25, 35, '2019-01-23 18:26:42');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `gradska`
--
ALTER TABLE `gradska`
  ADD CONSTRAINT `gradska_ibfk_1` FOREIGN KEY (`tip_karte`) REFERENCES `karta` (`tip_karte`);

--
-- Constraints for table `mjesecna`
--
ALTER TABLE `mjesecna`
  ADD CONSTRAINT `mjesecna_ibfk_1` FOREIGN KEY (`tip_karte`) REFERENCES `karta` (`tip_karte`);

--
-- Constraints for table `transakcije`
--
ALTER TABLE `transakcije`
  ADD CONSTRAINT `transakcije_ibfk_1` FOREIGN KEY (`id_karte`) REFERENCES `karta` (`id_karte`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
