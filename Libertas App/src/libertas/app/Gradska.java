/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package libertas.app;

import java.time.LocalDate;

/**
 *
 * @author IVICA'
 */
public class Gradska extends Karta{
    private int Cijena;
    

    public Gradska(LocalDate Datum, int id_karte, int PDV, int Ukupno,int Cijena) {
        super(Datum, id_karte, PDV, Ukupno);
        this.Cijena=Cijena;
    }

    /**
     * @return the Cijena
     */
    public int getCijena() {
        return Cijena;
    }

    /**
     * @param Cijena the Cijena to set
     */
    public void setCijena(int Cijena) {
        this.Cijena = Cijena;
    }
  
    int getPorez() {
        return PDV;
    }
   
}
